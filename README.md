# Changebars Module

This module allows you to add "changebars" (vertical bars) in the margins of documents, signaling where changes have been made.

### Requirements
The `changebar` package (changebar.sty) must be installed.

### Usage
In `Document > Settings... > Modules`, select `Change bars` and click `Add` followed by `OK`.

Change bars will appear in the output only if `Document > Change Tracking > Show Changes in Output` is selected.

### Limitations
* The `Change bars` module currently does not work with XeTeX or dvipdfm output, and is untested with LuaTeX.
* If the LaTeX `dvipost` package is installed, `Change bars` will not work with any compilation method other than `PDF (pdflatex)`.

### Options
Please consult the package documentation for details regarding options to the `changebar` package. One commonly used option determines the placement of the change bars. The choices (fairly self-explanatory) are `outerbars`, `innerbars` (default), `leftbars` and `rightbars`. To specify the location, enter the corresponding option value in `Document > Settings... > Document Class > Class options > Custom:`.

### Tested configurations
* LyX 2.1.4
    + DVI
    + PDF (pdflatex)
    + PDF (ps2pdf)
    + Postscript
  